import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class task5140 {
    public static void main(String[] args) throws Exception {
        task1();
        task2();
        task3();
        task4();
        task5();
        task7();
        task8();
        task9();
        task10();

    }

    public static void task1() {
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(3);
        numbers.add(1);
        numbers.add(-1);
        numbers.add(9);
        numbers.add(8);
        numbers.add(7);
        numbers.add(-9);
        numbers.add(5);
        numbers.add(-2);
        System.err.println(numbers);

        numbers.sort((o1, o2) -> o1 - o2);
        System.out.println(numbers);
    }

    public static void task2() {
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(10);
        numbers.add(11);
        numbers.add(-1);
        numbers.add(99);
        numbers.add(88);
        numbers.add(7);
        numbers.add(9);
        numbers.add(101);
        numbers.add(100);
        ArrayList<Integer> numberCopy = new ArrayList<>();
        for (int i = 0; i < numbers.size(); i++) {
            if (numbers.get(i) > 9 && numbers.get(i) < 101) {
                numberCopy.add(numbers.get(i));
            }
        }

        System.out.println(numberCopy);
    }

    public static void task3() {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Do");
        colors.add("Cam");
        colors.add("Vang");
        colors.add("Luc");
        colors.add("Lam");
        int flag = 0;
        for (String s : colors) {
            if (s == "Vang") {
                flag = 1;
            }

        }
        if (flag == 0) {
            System.err.println("KO");
        } else {
            System.err.println("OK");
        }

    }

    public static void task4() {
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(3);
        numbers.add(1);
        numbers.add(-1);
        numbers.add(9);
        numbers.add(8);
        numbers.add(7);
        numbers.add(-9);
        numbers.add(5);
        numbers.add(-2);
        System.err.println(numbers);
        int tong = 0;
        for (int i : numbers) {
            tong += i;
        }
        System.err.println("Tong: " + tong);
    }

    public static void task5() {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Do");
        colors.add("Cam");
        colors.add("Vang");
        colors.add("Luc");
        colors.add("Lam");
        System.out.println(colors);
        colors.clear();
        System.out.println("clear " + colors);

    }

    public static void task7() {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Do");
        colors.add("Cam");
        colors.add("Vang");
        colors.add("Luc");
        colors.add("Lam");

        colors.add("Cham");
        colors.add("Tim");
        colors.add("Hong");
        colors.add("Hac");
        colors.add("La");
        System.out.println(colors);
        Collections.reverse(colors);
        System.out.println(colors);

    }

    public static void task8() {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Do");
        colors.add("Cam");
        colors.add("Vang");
        colors.add("Luc");
        colors.add("Lam");

        colors.add("Cham");
        colors.add("Tim");
        colors.add("Hong");
        colors.add("Hac");
        colors.add("La");
        System.out.println(colors);
        List<String> subColor = colors.subList(3, 7);
        System.out.println(subColor);
    }

    public static void task9() {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Do");
        colors.add("Cam");
        colors.add("Vang");
        colors.add("Luc");
        colors.add("Lam");

        colors.add("Cham");
        colors.add("Tim");
        colors.add("Hong");
        colors.add("Hac");
        colors.add("La");
        System.out.println(colors);
        Collections.swap(colors, 3, 7);
        System.out.println(colors);
    }

    public static void task10() {
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(2);
        numbers.add(3);
        System.out.println(numbers);
        ArrayList<Integer> numbers1 = new ArrayList<>();
        numbers1.add(1);
        numbers1.add(2);
        numbers1.add(3);
        numbers1.addAll(numbers1);
        System.out.println(numbers1);

    }

}
