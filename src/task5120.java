import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class task5120 {
    public static void main(String[] args) throws Exception {
        // task1
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Do");
        colors.add("Cam");
        colors.add("Vang");
        colors.add("Luc");
        colors.add("Lam");

        System.out.println(colors);
        // task2
        ArrayList<Integer> numbers1 = new ArrayList<>();
        numbers1.add(1);
        numbers1.add(2);
        numbers1.add(3);
        ArrayList<Integer> numbers2 = new ArrayList<>();
        numbers2.add(4);
        numbers2.add(5);
        numbers2.add(6);

        System.out.println(numbers1.get(0));
        int tong = 0;
        for (int i = 0; i < numbers1.size(); i++) {
            tong += numbers1.get(i);
        }
        for (int i = 0; i < numbers2.size(); i++) {
            tong += numbers2.get(i);
        }
        System.out.println(tong);
        // task3
        ArrayList<String> colorT3 = new ArrayList<>();
        colorT3.add("Do");
        colorT3.add("Cam");
        colorT3.add("Den");
        System.out.println("Task3: " + colorT3.size());
        // task4
        ArrayList<String> colorT4 = new ArrayList<>();
        colorT4.add("Vang");
        colorT4.add("Luc");
        colorT4.add("Lam");
        colorT4.add("Cham");
        colorT4.add("Tim");
        System.out.println("Task4: " + colorT4.get(3));
        // Task5
        ArrayList<String> colorT5 = new ArrayList<>();
        colorT5.add("Vang");
        colorT5.add("Luc");
        colorT5.add("Lam");
        colorT5.add("Cham");
        colorT5.add("Tim");
        System.out.println("Task5: " + colorT5.get(colorT5.size() - 1));
        // task6
        ArrayList<String> colorT6 = new ArrayList<>();
        colorT6.add("Vang");
        colorT6.add("Luc");
        colorT6.add("Lam");
        colorT6.add("Cham");
        colorT6.add("Tim");
        System.out.println(colorT6);
        colorT6.remove(colorT6.size() - 1);
        System.out.println(colorT6);
        // task7
        ArrayList<String> colorT7 = new ArrayList<>();
        colorT7.add("Vang");
        colorT7.add("Luc");
        colorT7.add("Lam");
        colorT7.add("Cham");
        colorT7.add("Tim");
        for (String i : colorT7) {
            System.out.print(i + " ");
        }
        // task8
        ArrayList<String> colorT8 = new ArrayList<>();
        colorT8.add("Vang");
        colorT8.add("Luc");
        colorT8.add("Lam");
        colorT8.add("Cham");
        colorT8.add("Tim");
        Iterator<String> st = colorT8.iterator();
        System.out.println("Task8");
        // st.next();
        for (int i = 0; i < colorT8.size(); i++) {
            System.out.print(st.next());
        }
        // task9
        ArrayList<String> colorT9 = new ArrayList<>();
        colorT9.add("Vang");
        colorT9.add("Luc");
        colorT9.add("Lam");
        colorT9.add("Cham");
        colorT9.add("Tim");
        System.out.println("task9");
        for (int i = 0; i < colorT9.size(); i++) {
            System.out.print(colorT9.get(i) + " ");
        }

        // task10
        ArrayList<String> colorT10 = new ArrayList<>();
        colorT10.add("Vang");
        colorT10.add("Luc");
        colorT10.add("Lam");
        colorT10.add("Cham");
        colorT10.add("Tim");
        System.out.println(colorT10);
        colorT10.add(0, "Hong");
        System.out.println("colorT10 sau khi add " + colorT10);
        // task11
        ArrayList<String> colorT11 = new ArrayList<>();
        colorT11.add("Vang");
        colorT11.add("Luc");
        colorT11.add("Lam");
        colorT11.add("Cham");
        colorT11.add("Tim");
        System.out.println(colorT11);
        colorT11.set(3, "cam");
        System.out.println(colorT11);
        // task12
        ArrayList<String> nameT12 = new ArrayList<>();
        nameT12.add("John");
        nameT12.add("Alice");
        nameT12.add("Bob");
        nameT12.add("Steve");
        nameT12.add("John");
        nameT12.add("Maria");
        System.out.println(nameT12);
        int index = nameT12.indexOf("Alice");
        int index1 = nameT12.indexOf("Mark");
        System.out.println(index);
        System.err.println(index1);
        // task13
        ArrayList<String> nameT13 = new ArrayList<>();
        nameT13.add("John");
        nameT13.add("Alice");
        nameT13.add("Bob");
        nameT13.add("Steve");
        nameT13.add("John");
        nameT13.add("Maria");
        System.out.println(nameT13);
        int index31 = nameT13.lastIndexOf("Steve");
        int index32 = nameT13.lastIndexOf("John");
        System.out.println(index31);
        System.err.println(index32);

    }

}
