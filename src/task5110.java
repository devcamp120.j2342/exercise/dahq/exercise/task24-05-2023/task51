
public class task5110 {
    public static void main(String[] args) throws Exception {
        // task1
        System.out.println("Task1: " + task1("ho"));
        // task2
        System.out.println("Task2: " + task2("word", "drow"));
        System.out.println("Task2: " + task2("java", "js"));
        // task4
        System.out.println("Task4: " + task4("java"));
        // task6
        System.out.println("Task6: " + task6("helllouu"));
        // task7
        System.out.println("Task7: " + task7("1234"));
        // task8
        System.out.println("task8: " + task8("hello", 'l', 't'));
        // task9
        System.out.println("Task9: " + task9("I am developer "));
        // task10
        System.out.println("Task10: " + task10("aba"));
        System.out.println("Task10: " + task10("abc"));
        // task3
        // System.out.println("task3: " + task3("Java"));
        // System.out.println("task3: " + task3("Haha"));
        // System.out.println("task3: " + task3("Devcamp"));

    }

    public static String task1(String s) {
        String result = "";
        for (int i = 0; i < s.length() - 1; i++) {
            if (s.charAt(i) == s.charAt(i + 1)) {
                result += s.charAt(i);
            }
        }
        if (result == "") {
            return "NO";
        }
        return result;
    }

    public static boolean task2(String s, String sub) {
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != sub.charAt(sub.length() - 1 - i)) {
                return false;
            }
        }
        return true;
    }

    public static String task4(String s) {
        String result = "";
        for (int i = s.length() - 1; i >= 0; i--) {
            result += s.charAt(i);
        }
        return result;
    }

    // public static void task5(String s) {
    // // if(s.charAt(1) instanceof Character){

    // // }
    // if(s.charAt(1) instanceof String){

    // }

    // }
    public static int task6(String s) {
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == 'a' || s.charAt(i) == 'e' || s.charAt(i) == 'o' || s.charAt(i) == 'u'
                    || s.charAt(i) == 'i') {
                count++;
            }
        }
        return count;
    }

    public static int task7(String s) {
        int result = Integer.parseInt(s);
        return result;
    }

    public static String task8(String s, char a, char b) {
        String result = s.replace(a, b);
        return result;
    }

    public static String task9(String s) {

        String[] array = s.split(" ");
        String result = "";
        for (int i = array.length - 1; i >= 0; i--) {
            result += array[i] + " ";
        }
        return result;

    }

    public static boolean task10(String s) {

        StringBuilder strBuilder = new StringBuilder(s);
        StringBuilder reversedStr = strBuilder.reverse();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != reversedStr.charAt(i)) {
                return false;
            }
        }
        return true;
    }

    // public static String task3(String s) {
    // int count = s.replaceAll("[^" + 's' + "]", "").length();
    // for (int i = 0; i < s.length(); i++) {

    // }

    // }

}
