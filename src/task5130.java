public class task5130 {
    public static void main(String[] args) throws Exception {
        // task2
        System.out.println("Task2: " + task2("Hello welcom"));
        // task3
        System.out.println("Task3: " + task3("hello", "welcom"));
        // task4
        System.out.println("Task4: " + task4("hello welcom", "hello"));
        // task5
        System.out.println("Task5: " + task5("Hello", 3));
        // task6
        System.out.println("Task6: " + task6("Xin chào Việt Nam", 'i'));
        // task7
        System.err.println("Task7: " + task7("Xn chào Việt Nam", 'i'));
        // task8
        System.out.println("Tasl8: " + task8("hello việt Nam"));
        // task9
        System.out.println("Task9: " + task9("aBc"));
        // task10
        System.out.println("Task10: " + task10("HeLloO"));
        // task11
        System.out.println("Task11: " + task11("AbcGhT"));
        // task14
        System.out.println("Task14: " + task14("Welcome", "helo"));
        // task16
        System.out.println("Task16: " + task16("ascii123"));
        System.out.println("Task16: " + task16("asci"));
        // task17
        System.out.println("Task17: " + task17("Abcdefghijklmnopq1"));
        System.out.println("Task17: " + task17("Abcdefghijklmnopq"));
        // task12
        System.out.println("Task12: " + task12("abcdefghgrt", 3));

    }

    public static int task2(String s) {
        String string[] = s.split(" ");
        return string.length;
    }

    public static String task3(String s1, String s2) {
        return s1 + s2;
    }

    public static boolean task4(String s1, String s2) {
        return s1.contains(s2);
    }

    public static char task5(String s, int i) {
        return s.charAt(i);
    }

    public static int task6(String s, char c) {
        int count = 0;
        s.toLowerCase();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == c) {
                count++;
            }
        }
        return count;
    }

    public static int task7(String s, char c) {
        int index = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == c) {
                index = i;
                break;
            }
        }
        return index;
    }

    public static String task8(String s) {
        return s.toUpperCase();
    }

    public static String task9(String s) {
        String result = "";
        for (int i = 0; i < s.length(); i++) {
            if ((int) s.charAt(i) < 96) {
                result += Character.toLowerCase(s.charAt(i));

            } else {

                result += Character.toUpperCase(s.charAt(i));
            }
        }
        return result;

    }

    public static int task10(String s) {
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            if ((int) s.charAt(i) < 96) {
                count++;
            }
        }
        return count;
    }

    public static String task11(String s) {
        String result = "";
        for (int i = 0; i < s.length(); i++) {
            if ((int) s.charAt(i) < 96) {
                result += s.charAt(i);
            }
        }
        return result;
    }

    public static String task14(String s1, String s2) {
        String result = "";
        if (s1.length() == s2.length()) {
            result = s1 + s2;
        } else {
            if (s1.length() > s2.length()) {
                result = s1.substring(s1.length() - s2.length(), s1.length()) + s2;
            } else {
                result = s2.substring(s2.length() - s1.length(), s2.length()) + s1;
            }
        }
        return result;
    }

    public static boolean task16(String s) {
        for (int i = 0; i < s.length(); i++) {
            if ((int) s.charAt(i) > 47 && (int) s.charAt(i) < 58) {
                return true;
            }

        }
        return false;
    }

    public static boolean task17(String s) {
        String regex = "^[A-Z]\\w{0,18}\\d$";
        return s.matches(regex);
    }

    public static String task12(String s, int n) {
        String result = "";
        if (s.length() % n == 0) {
            for (int i = 0; i < s.length(); i += n) {
                result += s.substring(i, i + n) + " ";
            }
        } else {
            for (int i = 0; i < s.length() - (s.length() % n); i += n) {
                result += s.substring(i, i + n) + " ";
            }
            result += s.substring(s.length() - (s.length() % n), s.length() - 1);
        }
        return result;
    }

}
